<!--
order: 1
-->

![Welcome to the OKExChain](../img/okexchain-image.jpg)

# Introduction

## What is OKExChain？

OKExChain is a set of open-source blockchain projects developed by OKEx, aiming to promote the landing of large-scale commercial applications based on blockchain technology. It gives each participating node the same rights, allowing users to launch a variety of decentralized applications smoothly, issue their digital assets, create their own digital asset trading pairs, and trade freely. The cross-chain technology is the key to achieving the landing. Through the cross-chain module, the value interconnectivity, user interconnectivity, and scenario application interconnectivity of blockchain can be realized simply and efficiently, so that we can co-construct the ecosystem and the value-added system.

![okexchain multi-chain](../img/multi-chain.jpg)

## What is OKExDEX？

OKExChain-OKExDEX, as the first project of the OKExChain ecosystem, is a middleware that can freely issue DEX. With the design concept of "everyone can create DEX", it provides various basic functions needed to operate a DEX. Compared with traditional DEX, it adopts full on-chain matching and on-chain order book management to make matching information more transparent and safe; compared with Ethereum-based projects, the matching engine based on OKExChain's call auction can achieve matching in seconds, which is similar to the operating experience of a centralized exchange. In addition, its unique design idea is not to build a DEX belonging to a certain stakeholder, but to provide a platform to achieve the separation of technology and operations. Just as Ethereum makes digital asset issuance easy  through smart contract technology, OKExChain lowers the barrier for operating digital asset trading pairs by OKExDEX.

![OKExDEX multi-operator relation](../img/operators.jpg)

## The OKT

Do you have OKT tokens? With OKT, you have the superpower to contribute to the security and governance of the OKExChain. Delegate your OKT to one or more of the 100 validators on the OKExChain blockchain to earn more OKT through Proof-of-Stake. You can also vote with your OKT to influence the future of the OKExChain through on-chain governance proposals.

Learn more about [being a delegator](../delegators/delegators-faq.html).



## OKExChain Explorers

These block explorers allow you to search, view and analyze OKExChain data&mdash;like blocks, transactions, validators, etc.

* [OKlink](https://www.oklink.com)


## OKExChain CLI

`okexchaincli` is a command-line interface that lets you interact with the OKExChain. `okexchaincli` is the only tool that supports 100% of the OKExChain features, including accounts, transfers, delegation, and governance. Learn more about `okexchaincli` with the [delegator's CLI guide](../delegators/delegators-guide-cli.html).

## Running a full-node on the OKExChain Testnet

In order to run a full-node for the OKExChain testnet, you must first [install `okexchaind`](../getting-start/install-okexchain.html). Then, follow [the guide](../getting-start/install-okexchain.html).

If you are looking to run a validator node, follow the [validator setup guide](../validators/validators-guide-cli.html).

## Join the Community

Have questions, comments, or new ideas? Participate in the OKExChain community through one of the following channels.

* [OKExChain Validator Chat](https://t.me/OKChainValidator)
* [OKExChain Developer Chat](https://t.me/okchaintech)



